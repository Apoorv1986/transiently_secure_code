\newcommand{\consistent}{\textsc{Transiently Secure Network Updates}}


\section{Transiently Secure Network Updates}
\label{sec:consistent}
\subsection{Overview}
\label{consistent:overview}

Our tool helps in performing transiently secure network updates from the SDN
controller to OpenFlow switches such that no transiently inconsistent
network state is reached which would lead to skipping of crucial
waypoints or causing forwarding loops.

The possible out of order network updates exist because a programmable
network is essentially an asynchronous distributed system: the
installation of new policies as well as the update of existing
policies need to be communicated from the control plane to the
dataplane elements (switches, universal nodes, appliances, etc.). This
transmission is performed over an asynchronous and unreliable network,
and hence, updates may actually arrive out of order.  Errors are
caused by many reasons: there are many reasons why an update or flow
modification message may not have the effects it was expected to have.
For example, different switches from different vendors may react
differently to certain events, or may not support all the expected
features, or not in the expected form.


To overcome above problems, we implement consistent network update
tool which ensures: (1) waypoint enforcement and (2) loop freedom.

The tool performs three steps. (1) Divides network update into
multiple minimum rounds.  (2) Sends barrier request at the end of each
round from the controller to the OpenFlow switches and receives
barrier replies from all of the OpenFlow switches updated in the
corresponding round.  (3) After receiving the last barrier request
from an OpenFlow switch, schedules another round of update and the
process repeats.

\subsection{Requirements and Installations}
\label{consistent:requirements}
\subsubsection{Requirements}
Consistent network Updates tool is implemented on top of the RYU
controller (OpenFlow~1.3 package), which uses python to implement the
controller.  We have carried out our experiments in Mininet.
Currently, our code can be run together with OpenFlow switches
supporting OpenFlow version~1.3.
\subsubsection{Installation }
The following two steps are necessary to install consistent network
update tool on the linux operating systems:

\begin{enumerate}
\item Download the RYU (which supports OpenFlow 1.3 version) from the
  following link:
  \url{https://github.com/osrg/ryu/wiki/OpenFlow_Tutorial}
\item Download the consistent network update source code, topologies
  and REST messages for configuration of switches.  Extract it by running:

\begin{lstlisting}
  tar -xvzf Consistent.tar.gz
\end{lstlisting}
\end{enumerate}

\subsection{How-to and Examples}

We will demonstrate how to carry out Wayup4 experiment with four
rounds of network update and Peacock5 with five rounds of network
update.


In the \textbf{Wayup4} experiment, we investigate the WayUp update
algorithm.  Network Update 4 is a WayUp network update, where the
controller creates a four round (maximum for WayUp) update schedule.
\begin{enumerate}
\item Start the controller:
\begin{lstlisting}
  cd /usr/local/lib/python2.7/dist-packages/ryu/app
  PYTHONPATH=. ryu-manager --verbose ofctl_rest_own.py
\end{lstlisting}
\item Start the topology in mininet:
\begin{lstlisting}
  sudo python topo_experiment3_update_wayup_04.py
\end{lstlisting}
\item Initialise a route through the network.  After the
  initialization of the route, all switches have the desired
  forwarding entries. Run the following command to provide a
  connection between hosts h1 and h2, which pushes the initial
  configuration to the switches.
\begin{lstlisting}
  ./rest_experiment3_ip_update_wayup_04_initial.sh 
\end{lstlisting}
\item Start a communication between hosts h1 and h2 for example by
  generating load with iperf:

\begin{lstlisting}
  mininet> xterm h1 h2
  h2$ iperf -s
  h1$ iperf -c 10.0.0.2 -t 600
\end{lstlisting}
\item Start capturing the the network traffic, for example, with
  wireshark:
\begin{lstlisting}
   sudo wireshark 
   # select interface "lo"
   # and then click capture
\end{lstlisting}
\item Perform the WayUp update by sending the corresponding REST -
  message to the controller, which divides the network update into
  minimum four rounds as per Wayup algorithm:
\begin{lstlisting}
./rest_experiment3_ip_update_wayup_04_own.sh
\end{lstlisting}
\item Wait until the update finishes. The update finishes when the
  controller stops printing debug output.
\item Stop capturing with wireshark.
\item Analysis requires the following steps.
  \begin{itemize}
  \item Look at the debug log of the controller.
  \item Look at the wireshark trace.
  \item Filter for the protocol "http" and can find REST-message,
    which is sent to the controller and immediately after the REST -
    message, we can find "OFPT\_FLOW\_MOD" - messages,
    "OFPT\_BARRIER\_REQUEST" - messages and "OFPT\_BARRIER\_REPLY" -
    messages in the trace.
  \end{itemize}
\end{enumerate}

In the \textbf{Peacock5} experiment, we investigate the Peacock update
algorithm. Peacock5 is a Peacock network update, where the SDN
controller creates a five round update schedule.

\begin{enumerate}
\item Start the controller:
\begin{lstlisting}
  cd /usr/local/lib/python2.7/dist-packages/ryu/app
  PYTHONPATH=. ryu-manager --verbose ofctl_rest_own.py
\end{lstlisting}
\item Start the topology in mininet:
\begin{lstlisting}
  sudo python topo_experiment4_update_peacock_05.py
\end{lstlisting}
\item Initialize a route through the network. After the initialization
  of the route all switches have the desired forwarding entries.  Run
  the following command to provide a connection between hosts h1 and
  h2, which pushes the initial configuration to the switches.
\begin{lstlisting}
  ./rest_experiment4_ip_update_peacock_05_initial.sh
\end{lstlisting}
\item Start a communication between h1 and h2, for example, by
  generating load with iperf:

\begin{lstlisting}
  mininet> xterm h1 h2
  h2$ iperf -s
  h1$ iperf -c 10.0.0.2 -t 600
\end{lstlisting}
\item Start capturing the the network traffic, for example, with
  wireshark:
\begin{lstlisting}
   sudo wireshark 
   # select interface "lo"
   # and then click capture
\end{lstlisting}
\item Perform the peacock update by sending the corresponding REST -
  message to the controller:
\begin{lstlisting}
  ./rest_experiment4_ip_update_peacock_05_own.sh
\end{lstlisting}
\item Wait until the update finishes. The update finishes when the
  controller stops printing debug output.
\item Stop capturing with wireshark.
\item Analysis requires the following steps.
  \begin{itemize}
  \item Look at the debug log of the controller.
  \item Look at the wireshark trace.
  \item Filter for the protocol "http" and can find REST-message,
    which is sent to the controller, immediately after the REST -
    message, we can find "OFPT\_FLOW\_MOD" - messages,
    "OFPT\_BARRIER\_REQUEST" - messages and "OFPT\_BARRIER\_REPLY" -
    messages in the trace.
  \end{itemize}
\end{enumerate}
\subsection{Results}

\begin{figure}[tbhp] \centering
 \includegraphics[width=0.6\textwidth]{figs/Consistent/Wayupduration.png}
 \caption{ Update Duration of Wayup Algorithms}
 \label{fig:consistent-1}
\end{figure}

\begin{figure}[tbhp] \centering
 \includegraphics[width=0.6\textwidth]{figs/Consistent/PeacockupdateDuration.png}
 \caption{ Update Duration of Peacock Algorithms}
 \label{fig:consistent-2}
\end{figure}
As shown in Figure~\ref{fig:consistent-1}, the \textbf{Wayup}
algorithm was compared with the original one, named `reference' in the
figure.  The Wayup algorithm was run in three settings: with 2, 3, and
4 round update.  The corresponding update durations in seconds are
depicted in Figure~\ref{fig:consistent-1}.

Similarly, the \textbf{Peacock} algorithm was compared with the
reference algorithm and with the Wayup algorithm using the the same
topology as before.  The Peacock algorithm was run in two settings:
with three-round update (Peacock 3) and five-round update (Peacock
5). The corresponding update durations in seconds are depicted in
Figure~\ref{fig:consistent-2} where the results of the Wayup algorithm
with four round of updates is shown as Wayup4.


